import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router";
import Layout from "./components/Layout/Layout";
import Cards from "./components/Cards/Cards/Cards";
import AboutUs from "./components/AboutUs/AboutUs";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={AboutUs}/>
          <Route path="/work" component={Cards} />
          <Route path="/news" render={() => <h1>News</h1>} />
          <Route path="/contact" render={() => <h1>Contact</h1>} />
          <Route render={() => <h1>No found!</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
