import React from 'react';

import NavigationItem from "./NavigationItem/NavigationItem";

import './NavigationItems.css';

const NavigationItems = () => (
  <ul className="NavigationItems">
    <NavigationItem to="/" exact>About us</NavigationItem>
    <NavigationItem to="/work">Work</NavigationItem>
    <NavigationItem to="/news">News</NavigationItem>
    <NavigationItem to="/contact">Contact</NavigationItem>
  </ul>
);

export default NavigationItems;