import React from 'react';

import './Footer.css';
import {NavLink} from "react-router-dom";


const Footer = () => {
  return (
    <footer className="Footer">
      <div className="flex">
        <NavLink to="/" className="mail">info@sitename.com</NavLink>
        <NavLink to="/" className="fb" />
        <NavLink to="/" className="vk" />
        <NavLink to="/" className="tg" />
        <NavLink to="/" className="ig" />
        <NavLink to="/" className="tel">(000) 777 777 7777</NavLink>
      </div>
    </footer>
  );
};

export default Footer;