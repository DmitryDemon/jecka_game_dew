import React from 'react';

import './Logo.css';
import logo from '../../assets/images/logo.png';
import {NavLink} from "react-router-dom";

const Logo = () => (
  <div className="Logo">
    <NavLink to="/" exact><img src={logo} alt="Logo" /></NavLink>
  </div>
);


export default Logo;