import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import windrose from '../../../assets/images/Windrose_Art.png';
import sintera from '../../../assets/images/Sintera_Art1.png';
import fabela from '../../../assets/images/Fabela_Art1.png';
import boe from '../../../assets/images/BOE_Art1.jpg';

import './Cards.css'

const styles = {
    card: {
        width: '20%',
        margin: '10px',
        transform: "skewX(-15deg)",
        flexGrow: 0,
    },
    bigCard: {
        transition: '5s',
        margin: '10px',
        transform: "skewX(-15deg)",
    },
    media: {
        height: 140,
        transform: "skewX(15deg)",
        width: "122%",
        marginLeft: "-19px"
    },
};


class MediaCard extends Component {

    state = {
        activeIndex: null
    };

    handleClick = (index) => this.setState({ activeIndex: index });

    render(){

        const { classes } = this.props;

    return (
        <div className='container'>
            <Card onClick={() => this.handleClick(1) } className={this.state.activeIndex === 1? classes.bigCard : classes.card}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={windrose}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                          Wind Rose
                        </Typography>
                        <Typography component="p">
                          Игра в стиле стимпанка и реальных событий на тему времён 17-18 века, в жанре Action, Rts, Rpg
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size="small" color="primary">
                        Share
                    </Button>
                    <Button size="small" color="primary">
                        Learn More
                    </Button>
                </CardActions>
            </Card>
            <Card  onClick={() => this.handleClick(2) } className={this.state.activeIndex === 2? classes.bigCard : classes.card}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={sintera}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                          Dark Sintera
                        </Typography>
                        <Typography component="p">
                          Игра в стиле космическая фантастика и фэнтези. Освещает события происходящие в вымышленной
                          вселенной, но имеет некоторые аллюзии на реальный мир.
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size="small" color="primary">
                        Share
                    </Button>
                    <Button size="small" color="primary">
                        Learn More
                    </Button>
                </CardActions>
            </Card>
            <Card  onClick={() => this.handleClick(3) } className={this.state.activeIndex === 3? classes.bigCard : classes.card}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={fabela}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                          Fabula de Mingem
                        </Typography>
                        <Typography component="p">
                          это история о людях, которые борются за существование в этом жестоком мире.
                          Даже магия не может сделать жизнь проще, даже напротив, сложнее..
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size="small" color="primary">
                        Share
                    </Button>
                    <Button size="small" color="primary">
                        Learn More
                    </Button>
                </CardActions>
            </Card>
            <Card  onClick={() => this.handleClick(4) } className={this.state.activeIndex === 4? classes.bigCard : classes.card}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={boe}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                          Battle of Empires 1919
                        </Typography>
                        <Typography component="p">
                          The game takes place in an alternative timeline in the era of the First World War
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size="small" color="primary">
                        Share
                    </Button>
                    <Button size="small" color="primary">
                        Learn More
                    </Button>
                </CardActions>
            </Card>
        </div>

    );
    }

}

MediaCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MediaCard);