import React from 'react';
import './TopPic.css'
const TopPic = () => {
    return (

           <div className="top-pic">
               <div className="pic-info">
                   <h1>FieriaGold</h1><br/>
                   <p>Игровая студия с большим опытом работы, специализирующаяся на разработке компьютерных игр.
                       <br/> Мы команда любящая свое дело.</p>
               </div>
           </div>

    );
};

export default TopPic;