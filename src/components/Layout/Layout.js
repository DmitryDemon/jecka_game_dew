import React, {Fragment} from 'react';
import Toolbar from "../Navigation/Toolbar/Toolbar";

import './Layout.css';
import Footer from "../Navigation/Footer/Footer";

const Layout = ({children}) => (
  <Fragment>
    <Toolbar/>
    <main className="Layout-Content">
      {children}
    </main>
    <Footer />
  </Fragment>
);

export default Layout;